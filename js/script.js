"use strict";

let firstNum, secondNum, operator; 

do {
    firstNum = prompt("Enter the first random number", firstNum);
    secondNum = prompt("Enter the second random number", secondNum);
    operator =  prompt("Select one out of four math operators (+ - * /)", operator);
} while (firstNum === "" || firstNum === null || isNaN(firstNum) ||
secondNum === "" || secondNum === null || isNaN(secondNum) || 
operator === null || operator === "" || 
operator !== "+" && operator !== "-" &&
operator !== "*" && operator !== "/"
);


let math = function(fNum, sNum, operation) {
// The first way of solving this task 
switch (operation) {
    case "+":
        return `${fNum} + ${sNum} = ${+fNum + +sNum}`;
    case "-":
        return `${fNum} - ${sNum} = ${fNum - sNum}`;
    case "*":
        return `${fNum} * ${sNum} = ${fNum * sNum}`;
    case "/":
        return `${fNum} / ${sNum} = ${fNum / sNum}`;
}

// The second way of solving this task
//
// if (operation === '+') {
//     console.log(`${fNum} + ${sNum} = ${fNum + sNum}`);
//     return;
// } else if (operation === "-") {
//     console.log(`${fNum} - ${sNum} = ${fNum - sNum}`);
//     return;
// } else if (operation === "*") {
//     console.log(`${fNum} * ${sNum} = ${fNum * sNum}`);
//     return;
// } else if (operation === "/") {
//     console.log(`${fNum} / ${sNum} = ${fNum / sNum}`);
//     return;
// }
// math (firstNum, secondNum, operator);
}
console.log(math (firstNum, secondNum, operator));
